FROM php:5.4-apache
RUN docker-php-ext-install mysql
RUN a2enmod rewrite
COPY . /var/www/html/

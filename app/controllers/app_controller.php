<?php
class AppController extends Controller {

	var $adminCurrentQuizz = "";
	var $adminCurrentQuizzName = "";
	var $adminCurrentRound = "";

	var $components = array('Session','Auth');
	var $helpers = array('Html','Ajax','Javascript','Session');

	function beforeFilter() {
		
		$this->Auth->userModel = 'User';
		$this->Auth->fields = array('username' => 'name', 'password' => 'password');
		$this->Auth->authError = "";
		$this->Auth->autoRedirect = true;
		$this->Auth->authorize = 'controller';
		$this->Auth->loginError = "Identifiant ou mot de passe incorrects.";

		//Action effectuées pour l'interface admin
		if(isset($this->params['prefix']) && $this->params['prefix'] == 'admin') {
			$this->layout = 'admin';

			$this->Auth->loginAction = '/admin/users/login';
			$this->Auth->loginRedirect = '/admin/QuizzSessions';
			$this->Auth->logoutRedirect = '/admin';
			

			$infoUser = $this->Session->read('Auth.User');
			
			if($infoUser) {
				//Gestion du Quizz courrant pour le back
				$tmpCurrentQuizz = $this->Session->read('currentQuizz');
				if(!empty($tmpCurrentQuizz)) {
					$this->adminCurrentQuizz = $this->Session->read('currentQuizz');
					$objModelTeam = ClassRegistry::init('QuizzSession');
					$this->adminCurrentQuizzName = $objModelTeam->getQuizzName($this->adminCurrentQuizz);
				}
				//Gestion de la variable de session pour la manche 
				$tmpCurrentRound = $this->Session->read('currentRound');
				if(!empty($tmpCurrentRound)) {
					$this->adminCurrentRound = $tmpCurrentRound;
				}
			}
		}
		else {//On est dans le cas du front
			$this->layout = 'front';
			$this->Auth->loginAction = '/users/login';
			$this->Auth->loginRedirect = '/QuizzSessions';
			$this->Auth->logoutRedirect = '/';

			$tmpCurrentQuizz = $this->Session->read('currentQuizz');
			//TODO controller qu'un quizz est selectionné sinon affichage erreur ou selection Quizz
			if(!empty($tmpCurrentQuizz)) {
				$this->adminCurrentQuizz = $this->Session->read('currentQuizz');
			}
		}
	}

	function beforeRender() {
		$this->set('adminCurrentQuizz',$this->adminCurrentQuizz);
		$this->set('adminCurrentQuizzName',$this->adminCurrentQuizzName);
		$this->set('adminCurrentRound',$this->adminCurrentRound);
	}

	function isAuthorized(){
		return true;
	}
}
?>

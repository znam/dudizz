<?php
class QuestionsController extends AppController {

	var $uses = Array("Question","RoundHasQuestion","Theme");

	function isAuthorized(){
		return true;
	}

	function index() {
		$listQuestion = $this->Question->find('all');
		pr($listQuestion);
	}

	function admin_index() {
		//On recupere la liste des thèmes
		$listThemes = $this->Theme->find('all',Array("recursive" => 0));
		$selectTheme = Array();
		foreach($listThemes as $theme) {
			$selectTheme[$theme['Theme']['id']] = $theme['Theme']['name'];
		}
		
		$this->set('selectTheme',$selectTheme);
		$this->set('formFrom','question');
	}

	function admin_add() {
		if(!empty($this->data) && !empty($this->data['Question']['theme_id'])) {
			//pr($this->data);
			$fileName = uniqid()."_".$this->data['Question']['file']['name'];
			move_uploaded_file($this->data['Question']['file']['tmp_name'],"IMAGES/".$fileName);
			$this->data['Question']['html'] = $fileName;
			
			$this->Question->save($this->data);
			//pr($this->data);
			if($this->data['Question']['formFrom'] == "round") {
				$this->RoundHasQuestion->addQuestionToRound($this->adminCurrentRound,$this->Question->id);
				$this->redirect(array('controller'=>'Rounds', 'action'=>'admin_index'));
			}
			else if($this->data['Question']['formFrom'] == "question") {
				$this->redirect(array('controller'=>'Questions', 'action'=>'admin_index'));
			}
		}
	}

}
?>
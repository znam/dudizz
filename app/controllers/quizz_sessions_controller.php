<?php
class QuizzSessionsController extends AppController {

	var $uses = Array("QuizzSession","Score","QuizzSessionHasRound","RoundHasQuestion","Question","Buzzer");

	function buzz() {
		$this->layout = null;

		$buzz = $this->Buzzer->find('first',array("order" => "timestamp"));
		
		//$buzzTeam = $this->Session->read('buzzTeam');
		//pr($buzzTeam);

		$teamInfo = $this->Score->find('first',array(
							"conditions" => array(
								"Score.id" => $buzz['Buzzer']['id_score'],
								"Score.red_card" => 0
							)
						)
					);

		/*if(empty($buzzTeam) && !empty($teamInfo)) {
			$this->Session->write("buzzTeam",$buzz['Buzzer']['id_score']);
			$this->set("play",1);
		}*/
		//$timeStamp = microtime();
                //$mtime = explode(" ",$timeStamp);
                //$mtime = $mtime[1] + $mtime[0];
		//$diff = $mtime - $buzz['Buzzer']['timestamp'];
		if(!empty($buzz) /*&& $diff < 7*/) {
			$this->set("message",$teamInfo['Team']['name']);
			//$this->set("message",$buzz['Buzzer']['timestamp']." - ".$mtime);	
		}
		//else if($diff > 7) {
		//	$this->Buzzer->emptyBuzz();
		//}
		//$this->Session->setFlash("test","buzzTeam");
	}

	function unBuzz() {
		//$this->Session->write("buzzTeam","");
		$this->Buzzer->emptyBuzz();
		//$this->redirect(array('controller'=>'QuizzSessions', 'action'=>'index'));
	}

	function isAuthorized(){
		$infoUser = $this->Session->read('Auth.User');
		
		//Gestion des acces au Back
		if(isset($this->params['prefix']) && $this->params['prefix'] == 'admin') {
			if($infoUser['group_id'] != 1 && $infoUser['group_id'] != 2 && $infoUser['group_id'] != 3) {
				return false;
			}
			else {
				return true;
			}
		}
		//Gestion des acces au front
		else {
			if($infoUser['group_id'] == 2 || $infoUser['group_id'] == 1) {
				return true;
			}
			else {
				return false;
			}
		}
	}

	function index() {
		//GESTION DES MESSAGES DIVERS
		if(isset($this->params['named']['message'])) {
			$this->_messages($this->params['named']['message'],$this->params['named']['team']);
		}
		//FIN GESTION DES MESSAGES

		$listEquipes = $this->Score->find('all',array(
						"conditions" => array(
							"Score.quizz_session_id" => $this->adminCurrentQuizz
						),
						"order" => "Score.point_score DESC",
						"recursive" => 0
					)
				);
		$this->set("listEquipes",$listEquipes);

		$sessionInfo = $this->QuizzSession->find('first',array(
								"conditions" => array(
									"QuizzSession.id" => $this->adminCurrentQuizz
								)
							)
						);
		$currentRound = $sessionInfo['QuizzSession']['current_round_id'];
		if(empty($currentRound)) {//On affiche la liste des manches
			$listeRound = $this->QuizzSessionHasRound->find('all',array(
									"conditions" => array(
										"QuizzSessionHasRound.quizz_session_id" => $this->adminCurrentQuizz
									)
								)
							);
			$this->set('type','rounds');
			$this->set("listeRound",$listeRound);
		}
		else {//On affiche la question courrante
			$this->set('type','question');
			$currentQuestionId = $sessionInfo['QuizzSession']['current_question_id'];
			$currentRoundId = $sessionInfo['QuizzSession']['current_round_id'];
			//On recuperes les info de la question courrante
			$infoQuestion = $this->Question->find('first',array(
									"conditions" => array(
										"Question.id" => $currentQuestionId
									)
								)
							);
			$this->set('infoQuestion',$infoQuestion);

		}
	}

	function nextQuestion() {
		$this->Session->write("buzzTeam","");
		$this->Buzzer->emptyBuzz();

		$currentQu = $this->QuizzSession->find('first',array(
								"conditions" => array(
									"QuizzSession.id" => $this->adminCurrentQuizz
								),
								"fields" => array(
									"QuizzSession.current_question_id",
									"QuizzSession.current_round_id"
								),
								"recursive" => -1
							)
						);
		$currentQuestionId = $currentQu['QuizzSession']['current_question_id'];
		$currentRoundId = $currentQu['QuizzSession']['current_round_id'];

		//On recupere les infos de la question courrante
		$currentQuHasRound = $this->RoundHasQuestion->find('first',array(
									'conditions' => array(
										'RoundHasQuestion.round_id' => $currentRoundId,
										'RoundHasQuestion.question_id' => $currentQuestionId
									),
									'order' => 'RoundHasQuestion.order',
									'recursive' => -1
								)
							);
		//On recupere les infos de la question suivante
		$nextQuestion = $this->RoundHasQuestion->find('first',array(
									'conditions' => array(
										'RoundHasQuestion.round_id' => $currentRoundId,
										'RoundHasQuestion.order' => $currentQuHasRound['RoundHasQuestion']['order']+1
									),
									'order' => 'RoundHasQuestion.order',
									'recursive' => -1
								)
							);
		if(isset($nextQuestion) && !empty($nextQuestion)) {
			//On enregistre dans la BDD QuizzSession l'id de la question suivante
			$infoMAJ = array(
				"id" => $this->adminCurrentQuizz,
				"current_question_id" => $nextQuestion['RoundHasQuestion']['question_id']
			);
			$this->QuizzSession->save($infoMAJ);
			$this->redirect(array('controller'=>'QuizzSessions', 'action'=>'index'));
		}
		else { //Il n'y a plus de question donc on reinitialise le choix de la Manche
			$infoMAJ = array(
				"id" => $this->adminCurrentQuizz,
				"current_question_id" => null,
				"current_round_id" => null
			);
			$this->QuizzSession->save($infoMAJ);
			$this->redirect(array('controller'=>'QuizzSessions', 'action'=>'index'));
		}
	}


	function admin_index() {
		$listQuizz = $this->QuizzSession->find('all',array('conditions',array('QuizzSession.state' => 1)));
		$this->set('listQuizz',$listQuizz);
		//$this->set('adminCurrentQuizz',$this->adminCurrentQuizz);
	}

	function admin_add() {
		if(!empty($this->data)) {
			$this->QuizzSession->save($this->data);
			$this->redirect(array('controller'=>'QuizzSessions', 'action'=>'admin_index'));
		}
	}

	function admin_select($idQuizz) {
		$this->Session->write("currentQuizz",$idQuizz);
		$this->Session->write("currentRound","");
		$this->redirect(array('controller'=>'QuizzSessions', 'action'=>'admin_index'));
	}

	function _messages($message,$team) {
		if($message == "rouge") {
			$this->Session->setFlash($team,"jaune");
		}
	}
}
?>

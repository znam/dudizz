<?php
class RoundsController extends AppController {

	var $uses = Array("Round","QuizzSessionHasRound","RoundHasQuestion","Theme","Question","QuizzSession");

	function liste() {
		$listRound = $this->Round->find('all');
	}

	function launch() {
		//On recupere la première question de la manche
		$firstQuestion = $this->RoundHasQuestion->find('first',array(
							'conditions' => array(
								'RoundHasQuestion.round_id' => $this->data['Round']['id']
							),
							'order' => 'RoundHasQuestion.order'
						)
					);

		//On enregistre dans la BDD la Manche qui a été selectionnée
		$infoMAJ = array(
			"id" => $this->adminCurrentQuizz,
			"current_round_id" => $this->data['Round']['id'],
			"current_question_id" => $firstQuestion['RoundHasQuestion']['question_id']
		);
		$this->QuizzSession->save($infoMAJ);

		//Retour a la page principale
		$this->redirect(array('controller'=>'QuizzSessions', 'action'=>'index'));
	}


	function admin_index() {

		$conditions = "WHERE hasRound.quizz_session_id=".$this->adminCurrentQuizz." AND hasRound.round_id=Round.id ORDER BY hasRound.order";
		$listRound = $this->Round->query(
			"SELECT Round.* FROM rounds as Round,quizz_session_has_rounds as hasRound ".$conditions
		);
		$this->set('listRound',$listRound);

		//Cas quand une manche a été selectionnée
		if(!empty($this->adminCurrentRound)) {
			//Liste des questions deja affectées
			$detailRound = $this->RoundHasQuestion->find('all',Array(
									'conditions' => Array(
										'round_id' => $this->adminCurrentRound
									),
									'order' => 'order'
								)
							);
			$this->set("detailRound",$detailRound);

			//On recupere la liste des thèmes
			$listThemes = $this->Theme->find('all',Array("recursive" => 0));
			$selectTheme = Array();
			foreach($listThemes as $theme) {
				$selectTheme[$theme['Theme']['id']] = $theme['Theme']['name'];
			}
			
			$this->set('selectTheme',$selectTheme);
		
			//Si un theme a été selectionné on renvoie la liste des question associées
			if(isset($this->params['named']['idTheme'])) {
				$this->data['Round']['idTheme'] = $this->params['named']['idTheme'];
			}

			$listQuestion = Array();
			if(isset($this->data['Round']['idTheme'])) {
				$listQuestion = $this->_getAvailableQuestions($this->data['Round']['idTheme']);
			}
			
			$this->set('listQuestion',$listQuestion);
			$this->set('formFrom','round');
			
		}
	}

	function _getAvailableQuestions($themeId) {
		$listAffectedQuestion = $this->Question->query("SELECT Question . *
						FROM questions AS Question, round_has_questions AS RoundHasQuestion, rounds AS Round, quizz_session_has_rounds AS QuizzSessionHasRound
							WHERE Question.id = RoundHasQuestion.question_id
							AND RoundHasQuestion.round_id = Round.id
							AND Round.id = QuizzSessionHasRound.round_id
							AND QuizzSessionHasRound.quizz_session_id = ".$this->adminCurrentQuizz);
		
		$listNotQu = Array();
		foreach($listAffectedQuestion as $AffQu) {
			array_push($listNotQu,$AffQu['Question']['id']);
		}
		$listQuestion = $this->Question->find('all',Array(
								'conditions' => Array(
									'theme_id' => $themeId,
									'NOT' => Array(
										'Question.id' =>$listNotQu
									)
								),
								'recursive' => 0
							)
						);
		return $listQuestion;
	}

	function admin_add() {
		if(!empty($this->data)) {
			$this->Round->save($this->data);

			//On recupere la derniere manche pour connaitre l'ordre a attribuer
			$conditions = "WHERE hasRound.quizz_session_id=".$this->adminCurrentQuizz." AND hasRound.round_id=Round.id ORDER BY hasRound.order DESC";
			$listRound = $this->Round->query(
				"SELECT Round.name,hasRound.order FROM rounds as Round,quizz_session_has_rounds as hasRound ".$conditions
			);
			if(!empty($listRound)) {
				$lastOrder = $listRound[0]['hasRound']['order'];
			}
			else {
				$lastOrder = 0;
			}

			//On enregistre l'association
			$dataQUAsso = array(
				'QuizzSessionHasRound' => Array(
					"quizz_session_id" => $this->adminCurrentQuizz,
					"round_id" => $this->Round->id,
					"order" => $lastOrder+1
				)
			);
			$this->QuizzSessionHasRound->save($dataQUAsso);
			$this->redirect(array('controller'=>'Rounds', 'action'=>'admin_index'));
		}
	}

	function admin_select($idRound) {
		$this->Session->write("currentRound",$idRound);
		$this->redirect(array('controller'=>'Rounds', 'action'=>'admin_index'));
	}

	function admin_addQuestion($idQuestion) {
		$infoQuestion = $this->Question->find('first',Array('conditions' => array('Question.id' => $idQuestion)));
		$idTheme = $infoQuestion['Question']['theme_id'];
			
		$this->RoundHasQuestion->addQuestionToRound($this->adminCurrentRound,$idQuestion);
		$this->redirect(array('controller'=>'Rounds', 'action'=>'admin_index','idTheme' => $idTheme));
	}

	function admin_delQuestion($idQuestion) {
		$this->RoundHasQuestion->delQuestionToRound($this->adminCurrentRound,$idQuestion);
		$this->redirect(array('controller'=>'Rounds', 'action'=>'admin_index'));
	}
	
	function admin_upQuestion($idQuestion) {
		$this->RoundHasQuestion->upQuestionInRound($idQuestion,$this->adminCurrentRound);
		$this->redirect(array('controller'=>'Rounds', 'action'=>'admin_index'));
	}

	function admin_downQuestion($idQuestion) {
		$this->RoundHasQuestion->downQuestionInRound($idQuestion,$this->adminCurrentRound);
		$this->redirect(array('controller'=>'Rounds', 'action'=>'admin_index'));
	}
}
?>
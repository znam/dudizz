<?php
class ScoresController extends AppController {

	function addPoint($teamId) {
		//On recupere les infos du score de l'equipe
		$infoScore = $this->Score->find('first',array(
						"conditions" => array(
							"Score.team_id" => $teamId,
							"Score.quizz_session_id" => $this->adminCurrentQuizz
						),
						"recursive" => -1
					)
				);
		
		$newScore = $infoScore['Score']['point_score']+1;
		
		$infoSave = array(
			"id" => $infoScore['Score']['id'],
			"point_score" => $newScore
		);
		
		$this->Score->save($infoSave);
		$this->redirect(array('controller'=>'QuizzSessions', 'action'=>'index'));
	}

	function addYellow($teamId) {
		$infoScore = $this->Score->find('first',array(
						"conditions" => array(
							"Score.team_id" => $teamId,
							"Score.quizz_session_id" => $this->adminCurrentQuizz
						),
						"recursive" => -1
					)
				);
		$newYellow = 1;
		if($infoScore['Score']['yellow_card'] == 1) {
			$newYellow = 0;
		}
		
		$infoSave = array(
			"id" => $infoScore['Score']['id'],
			"yellow_card" => $newYellow
		);
		$this->Score->save($infoSave);

		if($newYellow == 0) {
			$this->redirect(array('controller'=>'QuizzSessions', 'action'=>'index',"message" => "rouge","team" => $teamId));
		}
		else {
			$this->redirect(array('controller'=>'QuizzSessions', 'action'=>'index'));
		}
	}

	function addRed($teamId) {
		$infoScore = $this->Score->find('first',array(
						"conditions" => array(
							"Score.team_id" => $teamId,
							"Score.quizz_session_id" => $this->adminCurrentQuizz
						),
						"recursive" => -1
					)
				);
		if($infoScore['Score']['red_card'] == 0) {
			$infoSave = array(
				"id" => $infoScore['Score']['id'],
				"red_card" => 1
			);
			$this->Score->save($infoSave);
		}
		else {
			$infoSave = array(
				"id" => $infoScore['Score']['id'],
				"red_card" => 0
			);
			$this->Score->save($infoSave);
		}
		$this->redirect(array('controller'=>'QuizzSessions', 'action'=>'index'));
	}

	function moinsDeux($teamId) {
		$infoScore = $this->Score->find('first',array(
						"conditions" => array(
							"Score.team_id" => $teamId,
							"Score.quizz_session_id" => $this->adminCurrentQuizz
						),
						"recursive" => -1
					)
				);
		$newPt = $infoScore['Score']['point_score']-2;
		$infoSave = array(
				"id" => $infoScore['Score']['id'],
				"point_score" => $newPt
			);
		$this->Score->save($infoSave);
		$this->redirect(array('controller'=>'QuizzSessions', 'action'=>'index'));
	}
}
?>
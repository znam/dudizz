<?php
class TeamsController extends AppController {

	var $name = "Teams";
	var $uses = array("Team","Score");

	function isAuthorized(){
		$infoUser = $this->Session->read('Auth.User');
		if($infoUser['group_id'] != 1 && $infoUser['group_id'] != 2) {
			return false;
		}
		else {
			return true;
		}
	}

	function admin_index() {
		$listTeamsActif = $this->Team->find('all',array(
						"joins" => array( array(
							'table' => 'scores',
							'alias' => 'Score',
							'type' => 'LEFT',
							'conditions' => array(
								'Score.team_id = Team.id'
							))
						),
						"conditions" => array(
							"Score.quizz_session_id" => $this->adminCurrentQuizz)
						)
					);

		//Construction de la liste des equipes deja associées
		$conditions = Array();
		$listNot = Array();
		if(!empty($listTeamsActif)) {
			$inCond = "NOT IN(";
			foreach($listTeamsActif as $team) {
				$inCond .= $team['Team']['id'].',';
				array_push($listNot,$team['Team']['id']);
			}
			$inCond = substr($inCond,0,-1);
			$inCond .= ')';
			$conditions = Array(
					"conditions" => Array('NOT' => Array('id' => $listNot))
				);
		}
		
		$listTeamsInActif = $this->Team->find('all',$conditions);

		$this->set('listTeamsActif',$listTeamsActif);
		$this->set('listTeamsInActif',$listTeamsInActif);
	}

	function admin_add() {
		$this->Team->save($this->data);
		
		$dataScore = Array(
			"Score" => Array(
				"team_id" => $this->Team->id,
				"quizz_session_id" => $this->adminCurrentQuizz
			)
		);
		$this->Score->save($dataScore);
		$this->redirect(array('controller'=>'Teams', 'action'=>'admin_index'));
	}

	function admin_addTeam($idTeam) {
		//Ajouter un controle qui verifie que la variable de session est toujours valide
		$dataScore = Array(
			"Score" => Array(
				"team_id" => $idTeam,
				"quizz_session_id" => $this->adminCurrentQuizz
			)
		);
		
		$this->Score->save($dataScore);
		$this->redirect(array('controller'=>'Teams', 'action'=>'admin_index'));
	}

	function admin_delTeam($idTeam) {
		//Ajouter un controle qui verifie que la variable de session est toujours valide
		$this->Score->query("DELETE FROM scores WHERE team_id=".$idTeam." AND quizz_session_id=".$this->adminCurrentQuizz);
		$this->redirect(array('controller'=>'Teams', 'action'=>'admin_index'));
	}

	function admin_unbind($idTeam) {
		$this->Score->query("UPDATE scores SET ip=NULL WHERE team_id=".$idTeam." AND quizz_session_id=".$this->adminCurrentQuizz);
		$this->redirect(array('controller'=>'Teams', 'action'=>'admin_index'));
	}
}
?>
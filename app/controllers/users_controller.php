<?php
class UsersController extends AppController {

	function admin_login() {
		$this->layout = 'admin_connect';
	}

	function admin_logout() {
		$this->Session->setFlash("Vous êtes maintenant déconnecté.");
  		$this->redirect($this->Auth->logout());
	}

	function login() {
		$this->layout = 'admin_connect';
	}
}
?>
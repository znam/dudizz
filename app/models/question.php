<?php
class Question extends AppModel {
	
	var $hasMany = Array(
		'RoundHasQuestion' => Array(
			'className' => 'RoundHasQuestion',
			'foreignKey' => 'question_id',
			'order' => 'order'
		)
	);

	var $belongsTo = Array(
		'Theme' => Array(
			'className' => 'Theme',
			'foreignKey' => 'theme_id'
		)
	);
}
?>
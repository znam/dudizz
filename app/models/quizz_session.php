<?php
class QuizzSession extends AppModel {
	
	var $name = 'QuizzSession';
	var $recursive = 2;

	var $hasMany = array(
		'QuizzSessionHasRound' => Array(
			'className' => 'QuizzSessionHasRound',
			'foreignKey' => 'quizz_session_id'
		),
		'Score' => Array(
			'className' => 'Score',
			'foreignKey' => 'quizz_session_id'
		)
	);

	var $belongsTo = array(
		'AdminUser' => Array(
			'className' => 'User',
			'foreignKey' => 'user_id'
		)
	);

	function getQuizzName($idQuizz) {
		$teamName = $this->find('first',array("conditions" => array('QuizzSession.id' => $idQuizz)));
		return $teamName['QuizzSession']['name'];
	}
}
?>

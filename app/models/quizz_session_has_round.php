<?php
class QuizzSessionHasRound extends AppModel {
	
	var $name = 'QuizzSessionHasRound';
	var $belongsTo = Array(
		'QuizzSession' => Array(
			'className' => 'QuizzSession',
			'foreignKey' => 'quizz_session_id',
			'conditions' => array('QuizzSession.state' => 1)
 		),
		'Round' => Array(
			'className' => 'Round',
			'foreignKey' => 'round_id'
		)
	);
}
?>
<?php
class Round extends AppModel {
	
	var $recursive=2;

	var $hasMany = Array(
		'QuizzSessionHasRound' => Array(
			'className' => 'QuizzSessionHasRound',
			'foreignKey' => 'round_id'
		),
		'RoundHasQuestion' => Array(
			'className' => 'RoundHasQuestion',
			'foreignKey' => 'round_id'
		)
	);
}
?>
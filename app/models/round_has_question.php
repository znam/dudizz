<?php
class RoundHasQuestion extends AppModel {
	
	var $belongsTo = Array(
		'Round' => Array(
			'className' => 'Round',
			'foreignKey' => 'round_id'
		),
		'Question' => Array(
			'className'=> 'Question',
			'foreignKey' => 'question_id'
		)
	);

	function addQuestionToRound($idRound,$idQuestion) {
		//On recupere l'ordre de la dernière question
		$lastQuestion = $this->find('first',Array(
					'conditions' => Array(
						'round_id' => $idRound
					),
					'order' => 'RoundHasQuestion.order DESC'
				)
			);
		$lastOrder = 0;
		if(!empty($lastQuestion)) {
			$lastOrder = $lastQuestion['RoundHasQuestion']['order'];
		}
		$dataInsert = Array(
			"RoundHasQuestion" => Array(
					"round_id" => $idRound,
					"question_id" => $idQuestion,
					"order" => $lastOrder+1
				)
		);
		//pr($dataInsert);
		$this->save($dataInsert);
	}

	function delQuestionToRound($idRound,$idQuestion) {
		$this->query("DELETE FROM round_has_questions WHERE round_id=".$idRound." AND question_id=".$idQuestion);
		//Penser a decrementer toutes les questions qui ont un ordre au dessus
	}

	function upQuestionInRound($idQuestion,$idRound) {
		$infoQu = $this->find('first',array('conditions' => array('round_id' => $idRound,'question_id' => $idQuestion)));
		$order = $infoQu['RoundHasQuestion']['order'];
		$infoQuUp = $this->find('first',array('conditions' => array('round_id' => $idRound,'order' => $order-1)));
		$infoQu['RoundHasQuestion']['order'] = $order-1;
		$infoQuUp['RoundHasQuestion']['order'] = $order;
		$this->save($infoQu);
		$this->save($infoQuUp);
	}

	function downQuestionInRound($idQuestion,$idRound) {
		$infoQu = $this->find('first',array('conditions' => array('round_id' => $idRound,'question_id' => $idQuestion)));
		$order = $infoQu['RoundHasQuestion']['order'];
		$infoQuUp = $this->find('first',array('conditions' => array('round_id' => $idRound,'order' => $order+1)));
		$infoQu['RoundHasQuestion']['order'] = $order+1;
		$infoQuUp['RoundHasQuestion']['order'] = $order;
		$this->save($infoQu);
		$this->save($infoQuUp);
	}
}
?>
<?php
class Score extends AppModel {
	
	var $recursive=2;

	var $belongsTo = Array(
		'Team' => Array(
			'className' => 'Team',
			'foreignKey' => 'team_id'
		),
		'QuizzSession' => Array(
			'className' => 'QuizzSession',
			'foreignKey' => 'quizz_session_id'
		)
	);

}
?>
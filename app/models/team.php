<?php
class Team extends AppModel {
	
	var $recursive=2;

	var $hasMany = Array(
		'Score' => Array(
			'className' => 'Score',
			'foreignKey' => 'team_id'
		)
	);

	/* Fonction renvoyant le nom d'une equipe pour un id donné */
	function getTeamName($idTeam) {
		$teamName = $this->find('first',array("conditions" => array('id' => $idTeam)));
		return $teamName['Team']['name'];
	}
}
?>
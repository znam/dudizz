<?php 
//Formulaire d'ajout de question
echo $form->create('Question', array('action'=>'add','type' => 'file')); 
echo $form->hidden('Question.formFrom', array('value'=>$formFrom));

$listType = Array(
	'1' => "Texte",
	'2' => "Image",
	'3' => "Vidéo",
	'4' => "Musique"
);
?>

<script>
	function changeDisplay() {
		var idType = document.getElementById('QuestionTypeId').value;
		if(idType != 1) {
			document.getElementById('fichier').style.display="block";
		}
		else {
			document.getElementById('fichier').style.display="none";
		}
	}
</script>

<table class="tableau border">
	<TR><TD colspan="2"><div class="headTab">Ajouter une question</div></TD></TR>
	<tr align="center"><td>
		Thème : <?=$form->select('Question.theme_id',$selectTheme)?>
		</td><td>
		Type : <?=$form->select('Question.type_id',$listType,1,Array('onChange' => 'changeDisplay();'))?>
	</td></tr>
	<tr><td>
		<?=$form->input('Question.question_text', array('label'=>'Question : ','class' => 'textQu'))?>
		</td><td>
		<?=$form->input('Question.answer_text', array('label'=>'Reponse : ','class' => 'textQu'))?>
	</td></tr>
	<tr><TD colspan="2" align="center">
		<div id="fichier" style="display:none">
		<?php
		echo $this->Form->file('Question.file');
		?>
		</div>
	</TD></tr>
	<tr><TD colspan="2"><?=$form->end('Ajouter');?></TD></tr>
</table>
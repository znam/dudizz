<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<?php echo $html->charset(); ?>
		<title><?php __('.:: Intranet de commande ::.'); ?></title>
				
		<?php
		//////////////////////
		//CHARGEMENT DES CSS//
		//////////////////////
		$aCss = array(
			'960/960',
			'960/text',
			'960/reset',
			'connect/commun',
			'connect/'.Configure::read('sCodeUrl'),
			'front_messages_flash'
		);
		
		echo $html->css($aCss);
		
		/////////////////////
		//CHARGEMENT DES JS//
		/////////////////////
		$aJs = array(
			'/js/libs/jquery-1-3-min',
			'/js/functions/front/init',
			'/js/functions/front/default',
			'/js/functions/front/ajax_connect'
		);
		
		echo $javascript->link($aJs);
		?>
	</head>

	<body>		
		<table class="connect_tableau">			
			<?php if(substr_count($_SERVER['HTTP_USER_AGENT'], "MSIE")) {  ?>
			<tr><td colspan="4">&nbsp;</td></tr>
			<tr>
				<td>&nbsp;</td>
				<td colspan="2"><b><?php echo $html->link(__("POUR UNE ERGONOMIE OPTIMALE, TELECHARGER MOZILLA FIREFOX", true), 'http://download.mozilla.org/?product=firefox-3.5.5&os=win&lang=fr', array('title' => __("POUR UNE ERGONOMIE OPTIMALE, TELECHARGER MOZILLA FIREFOX", true), 'class' => 'blank'), null, false); ?></b></td>
				<td>&nbsp;</td>
			</tr>
			<?php } ?>
			
			<tr class="ligne_js"><td colspan="4">&nbsp;</td></tr>
			<tr class="ligne_js">
				<td>&nbsp;</td>
				<td colspan="2"><b><?php __("VOUS DEVEZ ACTIVER LE JAVASCRIPT POUR QUE L'INTRANET FONCTIONNE CORRECTEMENT"); ?></b></td>
				<td>&nbsp;</td>
			</tr>
			
			<tr><td colspan="4">&nbsp;</td></tr>
			<tr>
				<td>&nbsp;</td>
				<td colspan="2" align="center"><b><?php __("INTRANET EN COURS DE DEVELOPPEMENT<br />NE PAS PASSER DE COMMANDE CAR CELLES-CI NE SERONT PAS TRAITEES"); ?></b></td>
				<td>&nbsp;</td>
			</tr>
			
			<tr>
				<td>&nbsp;</td>
				<td class="connect_logo"></td>
				<td class="connect_content"><?php echo $content_for_layout; ?></td>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td colspan="2" align="right"><span class="lost"><?php __('Mot de passe oublié?'); ?></span></td>
				<td>&nbsp;</td>
			</tr>
			<tr><td colspan="4">&nbsp;</td></tr>
			<?php if(isset($message_confirm)) { ?>
			<tr>
				<td>&nbsp;</td>
				<td colspan="2" align="right"><?php echo $message_confirm; ?></td>
				<td>&nbsp;</td>
			</tr>
			<?php } ?>
			<tr class="form_lost"><td colspan="4">&nbsp;</td></tr>
			<tr class="form_lost">
				<td>&nbsp;</td>
				<td class="mdp_content" colspan="2" align="right">
				<?php //echo $form->create(null, array('action' => 'login')); ?>
				<?php //echo $form->input('User.type_action', array('type' => 'hidden', 'value' => 'lost_passwd'));  ?>
					<p class="lost_passwd_identifiant grid_4 alpha"><?php __("Saisissez votre identifiant pour recevoir votre nouveau mot de passe par email"); ?> : <?=$form->input('User.login', array('div' => false, 'label' => false, 'error' => false))?></p>
					<p class="lost_passwd_valid grid_2 omega"><?=$form->submit('CONNECT/'.Configure::read('sCodeUrl').'/b_valid.gif',  array('class' => "noBorder", 'div' => false))?></p>
				<?php //$form->end(); ?>
				</td>
				<td>&nbsp;</td>
			</tr>
			<tr><td colspan="4">&nbsp;</td></tr>			
		</table>
		
		<div class="bas_page"></div>
	</body>
</html>
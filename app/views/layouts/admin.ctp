<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<?php 
		echo $html->charset(); 
		$aCss = array('tableau.css');
		echo $html->css($aCss);
	?>
	<title>Interface d'Administration</title>
</head>

<body>
	<?php 
		$session->flash();
		echo $session->flash('auth');
	?>
	<table width="100%">
		<tr><td>Application Quizz : <?=$adminCurrentQuizzName?></td></tr>
		<tr><td>
			<?php echo $this->element('admin/menu'); ?>
		</td></tr>
		<tr><td align="center">
			<?php echo $content_for_layout; ?>
		</td></tr>
	</table>
<?php //echo $this->element('sql_dump'); ?>
</body>
</html>

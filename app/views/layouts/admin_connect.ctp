<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<?php 
		echo $html->charset(); 
		$aCss = array('tableau.css');
		echo $html->css($aCss);
	?>
	<title>Interface d'Administration</title>
</head>

<body>
	<?php echo $session->flash('auth'); ?>
	<table>
		<tr><td>Authentification</td></tr>
		<tr><td>
			<?php echo $content_for_layout; ?>
		</td></tr>
	</table>
<?php echo $this->element('sql_dump'); ?>
</body>
</html>

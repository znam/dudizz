<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<?php echo $html->charset(); ?>
		<title><?php __('.:: Intranet de commande ::.'); ?></title>
				
		<?php
		//////////////////////
		//CHARGEMENT DES CSS//
		//////////////////////
		$aCss = array(
			'960/960',
			'960/text',
			'960/reset',
			'connect/commun',
			'connect/'.Configure::read('sCodeUrl')
		);
		
		echo $html->css($aCss);
		?>
	</head>

	<body>
		<table class="connect_tableau">
			<tr>
				<td></td>
				<td class="connect_logo"></td>
				<td class="connect_content"><?php echo $content_for_layout; ?></td>
				<td></td>
			</tr>			
		</table>
	</body>
</html>
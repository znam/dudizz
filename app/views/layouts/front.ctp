<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<title>Plateforme QUIZZ</title>
	<?php
	echo $html->charset();
	$aCss = array('tableauFront.css');
	echo $html->css($aCss);
	echo $javascript->link('prototype');
	echo $javascript->link('swfobject');
	?>
</head>

<body>
	<?php
	echo $ajax->remoteTimer(
		array(
			'url' => array( 'controller' => 'QuizzSessions', 'action' => 'buzz' ),
			'update' => 'divtest',
			'frequency' => 0.5
		)
	);
	?>
	<div id="divtest"></div>
	<?=$session->flash()?>
	<table height="800" width="100%" class="tableau" STYLE="background-image: url(img/fond_dude.jpg); background-repeat: no-repeat; background-position: center;">
		<TR>
			<TD width="230" valign="top" class="team">
				<div class="teamDiv">
				<p><br>Quizz du Dude</p>
				<?php echo $this->element('front/listeEquipes'); ?>
				</div>
			</TD>
			<td valign="top" class="question">
				<?php echo $content_for_layout; ?>
			</td>
		</TR>
	</table>
	<?php //echo $this->element('sql_dump'); ?>
	<?php /*
	<object data="player/dewplayer.swf" width="200" height="20" name="dewplayer" id="dewplayer" type="application/x-shockwave-flash">
		<param name="movie" value="player/dewplayer.swf" />
		<param name="flashvars" value="mp3=test.mp3&javascript=on" />
		<param name="wmode" value="transparent" />
	</object>
	
	<script type="text/javascript">

		var flashvars = {
			mp3: "test1.mp3",
			javascript: "on"
		};
		var params = {
			wmode: "transparent"
		};
		var attributes = {
			id: "dewplayer"
		};
		swfobject.embedSWF("dewplayer.swf", "dewplayer_content", "200", "20", "9.0.0", false, flashvars, params, attributes);

	</script>
	*/ ?>
</body>
</html>

<table class="tableau"><tr class="entete"><td colspan=2>Liste des Quizz</td></tr>
<?php
foreach($listQuizz as $quizz) {
	$classQuizz = "";
	if($quizz['QuizzSession']['id'] == $adminCurrentQuizz) {
		$classQuizz = "bg_1";
	}
	?>
	<tr class=<?=$classQuizz?>>
		<td>
			<?php echo $this->Html->link($quizz['QuizzSession']['name'],array('controller' => 'QuizzSessions','action' => 'select',$quizz['QuizzSession']['id'])); ?>
		</td>
	</tr>
	<?php
}
echo "</table><br><br>";

echo $form->create('QuizzSession', array('action'=>'add'));
echo $form->input('QuizzSession.name', array('label'=>'Nom du Quizz : '));
echo $form->end('Ajouter');
?>
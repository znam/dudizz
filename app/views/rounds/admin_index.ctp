<?php echo $form->create('Round', array('action'=>'add')); ?>
<table class="tableau border">
	<tr><TD><div class="headTab">Ajout d'une Manche</div></TD></tr>
	<tr><td><?php echo $form->input('Round.name', array('label'=>'Nom de la Manche: ')); ?></td></tr>
	<tr><TD><?php echo $form->end('Ajouter'); ?></TD></tr>
</table>
<br>

<table class="tableau">
	<tr><td><div class="headTab">Liste des Manches</div></td></tr>
	<?php 
	foreach($listRound as $round) { 
		$classRound = "";
		if($round['Round']['id'] == $adminCurrentRound) {
			$classRound = "bg_1";
		}
	?>
		<tr class="<?=$classRound?>">
			<td align="center"><?php echo $this->Html->link($round['Round']['name'],array('controller' => 'Rounds','action' => 'select',$round['Round']['id'])); ?></td>
		</tr>
	<?php } ?>
</table>

<?php
if(isset($adminCurrentRound) && !empty($adminCurrentRound)) { ?>
	
	<?php echo $this->element('admin/formQuestion'); ?>

	<br><br>
	<table class="tableau">
		<TR><TD colspan="4"><div class="headTab">Questions Selectionnées</div></TD></TR>
		<?php
		$cptFleches = 0;
		foreach($detailRound as $question) { ?>
			<tr>
				<TD> --> <i><?=$question['RoundHasQuestion']['order']?></i> : <?=$question['Question']['question_text']?></TD>
				<td class="status">
					<?php if($cptFleches != 0) {
						echo $html->image("haut.png",array('url' => array("controller" => 'Rounds','action' => 'upQuestion',$question['Question']['id'])));
					}?>
				</td>
				<td class="status">
					<?php if($cptFleches != (sizeof($detailRound)-1)) {
						echo $html->image("bas.png",array('url' => array("controller" => 'Rounds','action' => 'downQuestion',$question['Question']['id'])));
					} ?>
				</td>
				<td class="status"><?=$this->Html->link('Supr',array('controller' => 'Rounds','action' => 'delQuestion',$question['Question']['id']));?></td>
			</tr>
		<?php $cptFleches++; } ?>	
	</table>

	<?=$form->create('Round', array('action'=>'index'))?>
	<table class="tableau">
		<TR>
			<TD colspan="2"><div class="headTab">
				<?php
				echo 'Choisir un Thème : '.$this->Form->select('idTheme',$selectTheme,null,Array('onChange' => 'javascript:this.form.submit()'));
				
				?>
			</div></TD>
		</TR>
		<?php
		foreach($listQuestion as $question) { ?>
			<tr>
				<TD><?=$question['Question']['question_text']?></TD>
				<td class="status"><?=$this->Html->link('Ajout',array('controller' => 'Rounds','action' => 'addQuestion',$question['Question']['id']));?></td>
			</tr>
		<?php } ?>
	</table>
	<?=$form->end()?>

<?php } ?>
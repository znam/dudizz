<?php echo $form->create('Team', array('action'=>'add')); ?>
<table>
	<tr><td><?php echo $form->input('Team.name', array('label'=>'Nom Equipe: ')); ?></td></tr>
</table>
<?php echo $form->end('Ajouter'); ?>

<br><br>

<table class="tableau">
	<tr class="entete"><td>Liste des Equipes</td><td class="bind">ip</td><td class="status">Status</td></tr>
	<?php
	foreach($listTeamsActif as $team) { ?>
		<tr>
			<td><?=$team['Team']['name']?></td>
			<td class="bind"><?php echo $this->Html->link($team['Score'][0]['ip'], array('controller' => 'Teams', 'action' => 'admin_unbind', $team['Team']['id'])); ?></td>
			<td><?php echo $this->Html->link('Supr',array('controller' => 'Teams','action' => 'delTeam',$team['Team']['id'])); ?></td>
		</tr>
	<?php } ?>
		<tr class="bg_1"><TD align="center" colspan="3"><b>Equipes Inactives</b></TD></tr>
	<?php
	foreach($listTeamsInActif as $teamInA) { ?>
		<tr>
			<TD><?=$teamInA['Team']['name']?></TD>
			<td></td>
			<td><?php echo $this->Html->link('Ajout',array('controller' => 'Teams','action' => 'addTeam',$teamInA['Team']['id'])); ?></td>
		</tr>
	<?php } ?>
</table>
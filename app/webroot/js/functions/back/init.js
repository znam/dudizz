$(document).ready(function() {
	$(".blank").attr("target", "_blank"); //Mise en place des target valides xhtml strict	
	$(".ft_js_ext").hide(); //On cache le message quand le js est active
	
	/*GESTION DE LA LISTE DES MAGASINS*/		
	$(".ft_list_mags ul").hide();	//On cache tous les sous menus	
	$(".ft_list_mags").mouseover(function() { $(".ft_list_mags ul").show(); }).mouseout(function() { $(".ft_list_mags ul").hide(); });
	
	/*GESTION DES SOUS MENUS*/		
	$(".ft_sous_menu").hide();	//On cache tous les sous menus	
	$(".ft_menu").mouseover(function() { 
		$(this).children(".ft_sous_menu").show();
	}).mouseout(function() { 
		$(".ft_sous_menu").hide(); 
	});
		
	/*BARRE DE SCROLL*/
	//var iScroolPaneWidth = $('.ft_scroll_pane').width();
	//iScroolPaneWidth = iScroolPaneWidth + 17; //17 = width de la barre de scrool standard
	//$('.ft_scroll_pane').width(iScroolPaneWidth);
	$('.ft_scroll_pane').jScrollPane({scrollbarWidth:5, scrollbarMargin:5});	
	
	/*GESTION DES CHAMPS DATE*/	
	$('.form_date').datepicker($.datepicker.regional['fr']);	
	
	//hover states on the static widgets
	$('#dialog_link, ul#icons li').hover(
		function() { $(this).addClass('ui-state-hover'); }, 
		function() { $(this).removeClass('ui-state-hover'); }
	);
	
	/*GESTION DES MENUS ACCORDION*/
	$(".ft_accordion").accordion();
	
	
	
	/** AJAX ADD ARTICLES **/
	$('.forAjax').change(
		function() {
			var sValeurId = $(this).attr("id");
			var sValeurChoix = $(this).val();
			//alert(sValeurId+'  '+sValeurChoix);
			switch(sValeurId){
				case 'ArticleBaseDocumentaryId' : 
					if(sValeurChoix != '') {  
						$.post("listArticlesSectorAjax", {id: sValeurChoix}, function (data) { 
							if(data) {
								$("#divLSArticlesSector").html(data)
								$("#divLSArticlesSector").show('fast');
								$("#listeArticlesSector").show('slow');
								$("#listeArticlesSubSector").hide('slow'); 
							} else { 
								$("#divLSArticlesSector").html('<input type="hidden" name="data[Article][articles_sector_id]" value="">');
								$("#listeArticlesSubSector").html('<input type="hidden" name="data[Article][articles_sub_sector_id]" value="">');
								$("#divLSArticlesSector").hide('fast');
								$("#listeArticlesSector").hide('slow');
								$("#listeArticlesSubSector").hide('slow');
							}
						
						} );
					} else {
						$("#divLSArticlesSector").html('<input type="hidden" name="data[Article][articles_sector_id]" value="">');
						$("#listeArticlesSubSector").html('<input type="hidden" name="data[Article][articles_sub_sector_id]" value="">');
						$("#divLSArticlesSector").hide('fast');
						$("#listeArticlesSector").hide('slow');
						$("#listeArticlesSubSector").hide('slow');
					}
				break;
				
				case 'divLSArticlesSector':
					$.post("listArticlesSubSectorAjax", {id: $('#ArticleArticlesSectorId').val()}, function (data) { 
						if(data) {
							$("#listeArticlesSubSector").html(data)
							$("#listeArticlesSubSector").show('fast');
						} else { 
							$("#listeArticlesSubSector").html('<input type="hidden" name="data[Article][articles_sub_sector_id]" value="">');
							$("#listeArticlesSubSector").hide('slow'); 
						}
					} );
				break;
			}
		}
	);
	
	
	/** WYMEDITOR **/
	$('.wymeditor').wymeditor({
		lang:	"fr",
		logoHtml:  "",
		
   		boxHtml:   "<div class='wym_box'>"
             + "<div class='wym_area_top'>" 
             + WYMeditor.TOOLS
             + "</div>"
             + "<div class='wym_area_left'></div>"
             + "<div class='wym_area_right'>"
             + "</div>"
             + "<div class='wym_area_main'>"
             + WYMeditor.HTML
             + WYMeditor.IFRAME
             + WYMeditor.STATUS
             + "</div>"
             + "</div>",
		
		toolsItems: [
	        {'name': 'Bold', 'title': 'Strong', 'css': 'wym_tools_strong'}, 
	        {'name': 'Italic', 'title': 'Emphasis', 'css': 'wym_tools_emphasis'},
	        {'name': 'InsertOrderedList', 'title': 'Ordered_List',
	            'css': 'wym_tools_ordered_list'},
	        {'name': 'InsertUnorderedList', 'title': 'Unordered_List',
	            'css': 'wym_tools_unordered_list'},
	        {'name': 'Indent', 'title': 'Indent', 'css': 'wym_tools_indent'},
	        {'name': 'Outdent', 'title': 'Outdent', 'css': 'wym_tools_outdent'},
	        {'name': 'Undo', 'title': 'Undo', 'css': 'wym_tools_undo'},
	        {'name': 'Redo', 'title': 'Redo', 'css': 'wym_tools_redo'},
	        {'name': 'InsertTable', 'title': 'Table', 'css': 'wym_tools_table'},
	        {'name': 'Paste', 'title': 'Paste_From_Word','css': 'wym_tools_paste'}
	    ],
		
	   	containersItems: []
	});
	
	
	$('.checkAll').change(
		function() {
			var sObjectForm = $(this).parents("form");
			var sValeurFormId = $(sObjectForm).attr("id");
			$('#'+sValeurFormId+' input:checkbox').not( $('#all') ).each(  
		    	function() { if($('#all').attr('checked') == true) this.checked = true ; else this.checked = false; }  
			)  
		}
	);
	
});
$(document).ready(function() {

/** Carousel Emag - JCarouselLite */
	/*$(".jCarouselLite").jCarouselLite({
        btnNext: ".next",
        btnPrev: ".prev",
        visible: 6,
        scroll: 6,
        speed: 600
    });*/

/**************/
/*   GAUCHE   */
/**************/	
	var suppr_liens_focus = function () {
		var sUrlPage = $(this).attr('href'); //On r�cup�re le lien
		$(this).removeAttr('href');
		$(this).attr('lien', sUrlPage);
	};	
	$("#gauche a").livequery(suppr_liens_focus);
	
	$("#gauche a").live('click', function() { //Tous les liens de la home sauf ceux de la pagination (g�r� plus bas)
		
		var sUrlPage = $(this).attr('lien'); //On r�cup�re le lien
		//alert("GAUCHE : " + sUrlPage);
		
		$.get(sUrlPage, function(aData) { //On r�cup�re les donn�es en GET
			$(".ft_centre").empty().hide().html(aData).show(); //On affiche
		});	
		return false; //Emp�che que le lien soit suivit par HTML
	});	
/******************/
/*   FIN GAUCHE   */
/******************/	

/****************/
/*   A SAVOIR   */
/****************/	
	var gestion_articles = function () { $(".openCloseArticle").removeAttr("href"); }; // plie - d�plie article - page CMS 	
	$(".openCloseArticle").livequery(gestion_articles);
		
	 $(".openCloseArticle").live('click', function() {
			var idLien = $(this).attr("id");
			var temp = idLien.split('_');
			var sNamePopup = '#article_'+temp[1];
			var titre = $(this).html();
			
			var cssStyle = $(sNamePopup).attr("class");
			var isHidden = cssStyle.search(/hidden/);

			if(isHidden != -1) {
				$(sNamePopup).removeClass("hidden");
				$(sNamePopup).addClass("block");
				titre = titre.replace('&nbsp;&nbsp;|&nbsp;+ ','');
				$(this).html(titre+'&nbsp;&nbsp;|&nbsp;- ');	 
			} else {
				$(sNamePopup).removeClass("block");
				$(sNamePopup).addClass("hidden");
				titre = titre.replace('&nbsp;&nbsp;|&nbsp;- ','');
				$(this).html(titre+'&nbsp;&nbsp;|&nbsp;+ ');				
			}
	});	
	
	$('#form_affine_a_savoir').live('submit', function() {
				
		var sFormId = $(this).attr('id'); //On r�cup�re l'id du formulaire						
		var sAction = $(this).attr('action'); //Ainsi que l'action pour savoir ou renvoyer
			
		$.post(sAction, $(this).serialize(), function(aData) { //On POST les datas en ayant s�rializ� le formulaire
			$("#ajax_content").empty().hide().html(aData).show(); //On affiche				
		});
		
		return false; //Emp�che que le lien soit suivit par HTML
	});
/********************/
/*   FIN A SAVOIR   */
/********************/


/********************/
/*		EMAG 		*/
/********************/
	
	var gestion_emags = function () { 
		$(".jCarouselLite").jCarouselLite({
	        btnNext: ".next",
	        btnPrev: ".prev",
	        visible: 6,
	        scroll: 6,
	        speed: 600
	    });
	 }; // 
	$(".jCarouselLite").livequery(gestion_emags);
	

	
	
/********************/
/*	   FIN EMAG 	*/
/********************/


	
});
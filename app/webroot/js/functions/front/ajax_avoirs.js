$(document).ready(function() {

/****************************/
/*   AFFICHAGE DES AVOIRS   */
/****************************/
	//Formulaire de tri
	$('#form_avoirs_liste').live('submit', function() {
			
		var sFormId = $(this).attr('id'); //On r�cup�re l'id du formulaire			
		var sAction = $(this).attr('action'); //Ainsi que l'action pour savoir ou renvoyer
			
		$.post(sAction, $(this).serialize(), function(aData) { //On POST les datas en ayant s�rializ� le formulaire
			$("#ajax_content").empty().hide().html(aData).show(); //On affiche
		});
		return false; //Emp�che que le lien soit suivit par HTML
	
	});

	//Suppression des liens
	var suppr_liens_orders_historiques = function () {
		var sUrlPage = $(this).attr('href'); //On r�cup�re le lien		
		$(this).removeAttr('href');
		$(this).attr('lien', sUrlPage);
	};	
	$("#ft_avoirs_liste a").livequery(suppr_liens_orders_historiques);
	
	$('#ft_avoirs_liste a').live("click", function() {
	
		var sUrlPage = $(this).attr('lien'); //On r�cup�re le lien		
		$(location).attr('href', sUrlPage);					
		return false; //Emp�che que le lien soit suivit par HTML
	});
/********************************/
/*   FIN AFFICHAGE DES AVOIRS   */
/********************************/

});
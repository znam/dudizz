$(document).ready(function() {

/*************************************/
/*   GESTION DES DEMANDES D'AVOIRS   */
/*************************************/
	//deplie/replie les demandes d'avoirs
	var gestion_deplier_avoir = function () {
		$(this).removeAttr('href');
	};		
	$(".openCloseDemAvoir").livequery(gestion_deplier_avoir);
	
	$(".openCloseDemAvoir").live('click', function() {
		var idLien = $(this).attr("id");
		var temp = idLien.split('_');
		var sNamePopup = '#product_'+temp[1];
		var cssStyle = $(sNamePopup).attr("class");
		var isHidden = cssStyle.search(/saisie_demande_selected/);
	
		if(isHidden != -1) {
			//alert('oprn');
			//$("span.selected").removeClass("selected");
			$('#'+idLien+' span').removeClass("selected");
			$(sNamePopup).removeClass("saisie_demande_selected");
			$(sNamePopup).addClass("saisie_demande");
			
		} else {
			//alert('ici');
			$("span.selected").removeClass("selected");
			$(".saisie_demande_selected").addClass("saisie_demande");
			$(".saisie_demande_selected").removeClass("saisie_demande_selected");
			$('#'+idLien+' span').addClass("selected");
			$(sNamePopup).removeClass("saisie_demande");
			$(sNamePopup).addClass("saisie_demande_selected");
		}
	});
	
	//Soumission du formulaire
	$('#form_avoirs_demands_demande_avoir').live('submit', function() {
			
		var sFormId = $(this).attr('id'); //On r�cup�re l'id du formulaire			
		var sAction = $(this).attr('action'); //Ainsi que l'action pour savoir ou renvoyer
		
		//alert(sAction);
		$.post(sAction, $(this).serialize(), function(aData) { //On POST les datas en ayant s�rializ� le formulaire
			$("#ajax_content").empty().hide().html(aData).show(); //On affiche
		});
		return false; //Emp�che que le lien soit suivit par HTML
	
	});
/*****************************************/
/*   FIN GESTION DES DEMANDES D'AVOIRS   */
/*****************************************/

/***************************************/
/*   GESTION LISTE DEMANDES D'AVOIRS   */
/***************************************/
	
	//Formulaire de recherche
	$('#form_avoirs_demands_liste').live('submit', function() {
			
		var sFormId = $(this).attr('id'); //On r�cup�re l'id du formulaire			
		var sAction = $(this).attr('action'); //Ainsi que l'action pour savoir ou renvoyer
		
		//alert(sAction);
		$.post(sAction, $(this).serialize(), function(aData) { //On POST les datas en ayant s�rializ� le formulaire
			$("#ajax_content").empty().hide().html(aData).show(); //On affiche
		});
		return false; //Emp�che que le lien soit suivit par HTML
	
	});
	
	var suppr_liens_demande_avoir = function () {
		var sUrlPage = $(this).attr('href') + "/fromMenu:1"; //On r�cup�re le lien
		$(this).removeAttr('href');
		$(this).attr('lien', sUrlPage);
	};	
	$("#ft_avoirs_demands_liste a").livequery(suppr_liens_demande_avoir);
	
	$("#ft_avoirs_demands_liste a").live('click', function() { //Tous les liens de la home sauf ceux de la pagination (g�r� plus bas)
		
		var sUrlPage = $(this).attr('lien'); //On r�cup�re le lien
		var sCss = $(this).attr('class');
		if(sCss == 'blank') {
			$(location).attr('href', sUrlPage);	
		} else {
			$.get(sUrlPage, function(aData) { //On r�cup�re les donn�es en GET
				$("#ft_content").empty().hide().html(aData).show();
				
			});
		}
		return false; //Emp�che que le lien soit suivit par HTML
	});	

/*******************************************/
/*   FIN GESTION LISTE DEMANDES D'AVOIRS   */
/*******************************************/

});
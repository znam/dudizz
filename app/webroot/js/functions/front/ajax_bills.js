$(document).ready(function() {

/**************************/
/*   LISTE DES FACTURES   */
/**************************/
	//Formulaire de tri
	$('#form_bills_liste').live('submit', function() {
			
		var sFormId = $(this).attr('id'); //On r�cup�re l'id du formulaire			
		var sAction = $(this).attr('action'); //Ainsi que l'action pour savoir ou renvoyer
			
		$.post(sAction, $(this).serialize(), function(aData) { //On POST les datas en ayant s�rializ� le formulaire
			$("#ajax_content").empty().hide().html(aData).show(); //On affiche
		});
		return false; //Emp�che que le lien soit suivit par HTML
	
	});

	var suppr_liens_demande_avoir = function () {
		var sUrlPage = $(this).attr('href') + "/fromMenu:1"; //On r�cup�re le lien
		$(this).removeAttr('href');
		$(this).attr('lien', sUrlPage);
	};	
	$("#ft_bills_liste a").livequery(suppr_liens_demande_avoir);
	
	$("#ft_bills_liste a").live('click', function() { //Tous les liens de la home sauf ceux de la pagination (g�r� plus bas)
		
		var sUrlPage = $(this).attr('lien'); //On r�cup�re le lien
		var sCss = $(this).attr('class');
		if(sCss == 'blank') {
			$(location).attr('href', sUrlPage);	
		} else {
			$.get(sUrlPage, function(aData) { //On r�cup�re les donn�es en GET
				$("#ft_content").empty().hide().html(aData).show();
				
			});
		}
		return false; //Emp�che que le lien soit suivit par HTML
	});	
/******************************/
/*   FIN LISTE DES FACTURES   */
/******************************/

});
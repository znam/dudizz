$(document).ready(function() {

	/*$(location).attr('href',url); --> redirection*/
	
	/*location.hash = "#" + "toto";*/
	
/********************************************/
/*   MISE EN PLACE DES TARGET BLANK VALID   */
/********************************************/
	var target_blank = function () {
		$(".blank").attr("target", "_blank");
	};	
	$(".blank").livequery(target_blank);
/************************************************/
/*   FIN MISE EN PLACE DES TARGET BLANK VALID   */
/************************************************/

/*********************************************/
/*   GESTIONNAIRE DU CHANGEMENT DE MAGASIN   */
/*********************************************/
	//Suppression de tous les liens href
	$("#header_liste_magasins a").each(function() {
		var sUrlPage = $(this).attr('href'); //On r�cup�re le lien
		$(this).removeAttr('href');
		$(this).attr('lien', sUrlPage);
	});
	
	$('#header_liste_magasins a').click(function() {
	
		var sUrlPage = $(this).attr('lien'); //On r�cup�re le lien		
		
		$("#header_liste_magasins li").removeClass();
		$(this).parent().parent().addClass("ft_selected_store");
		$.get(sUrlPage, function(aData) { //On r�cup�re les donn�es en GET
			
			var sLienRedirection = $("#ft_menu_coup_coeur a").attr('lien');
			$.get(sLienRedirection, function(aData2) { //On r�cup�re les donn�es en GET
				$("#ft_content").empty().hide().html(aData2).show(); //On affiche
				$("#ft_ariane .ft_mag_connecte span").empty().html(aData); //On affiche
			});
					
		});
					
		return false; //Emp�che que le lien soit suivit par HTML
	});
/*************************************************/
/*   FIN GESTIONNAIRE DU CHANGEMENT DE MAGASIN   */
/*************************************************/

/*********************************/
/*   GESTIONNAIRE DU MENU HAUT   */
/*********************************/
	//Suppression de tous les liens href
	$("#menu_intranet a").not('.logout').each(function() {
		var sUrlPage = $(this).attr('href') + "/fromMenu:1"; //On r�cup�re le lien
		$(this).removeAttr('href');
		$(this).attr('lien', sUrlPage);
	});
	
	$('#menu_intranet a').not('.logout').click(function() {
	
		var sUrlPage = $(this).attr('lien'); //On r�cup�re le lien		
				
		$.get(sUrlPage, function(aData) { //On r�cup�re les donn�es en GET
			$("#ft_content").empty().hide().html(aData).show(); //On affiche
		});
					
		return false; //Emp�che que le lien soit suivit par HTML
	});
/*************************************/
/*   FIN GESTIONNAIRE DU MENU HAUT   */
/*************************************/
		
/**************************/
/*  AFFICHAGE DU LOADER   */
/**************************/	
	$.get($.get_host() + 'ajaxs/get_message_flash/AJAX_EN_COURS', function(aData) { //On r�cup�re les donn�es en GET
		$(aData).insertAfter(".bas_page").ajaxStart(function() {
			$(this).css($.get_surface_ecran()).show();
		}).ajaxStop(function(aData) {
			$(this).hide();
		});		
	});		
/******************************/
/*  FIN AFFICHAGE DU LOADER   */
/******************************/
		
/********************************************************************/
/*   GESTIONNAIRE DES ONGLETS PROPOSITIONS ET DETAILS PROPOSITIONS  */
/********************************************************************/
	//Suppression de tous les liens href
	var suppr_liens_onglets = function () {
		var sUrlPage = $(this).attr('href'); //On r�cup�re le lien
		$(this).removeAttr('href');
		$(this).attr('lien', sUrlPage);
	};	
	$("#sous_menus_page a").livequery(suppr_liens_onglets);
	
	$('#sous_menus_page a').live('click', function() {
	
		var sUrlPage = $(this).attr('lien'); //On r�cup�re le lien		
				
		$.get(sUrlPage, function(aData) { //On r�cup�re les donn�es en GET
			$("#ajax_content").empty().hide().html(aData).show(); //On affiche
		});
		$('#sous_menus_page li').removeClass("selected");
		$(this).parent().addClass("selected");			
		return false; //Emp�che que le lien soit suivit par HTML
	});
/************************************************************************/
/*   FIN GESTIONNAIRE DES ONGLETS PROPOSITIONS ET DETAILS PROPOSITIONS  */
/************************************************************************/
	
/*******************************************/
/*   CALCUL PRIX DE VENTE ORDERS ET BILLS  */
/*******************************************/

	/***************/
	/*   ETAPE 1   */
	/***************/			
	$('#form_calcul_pv').live('submit', function() {
					
		var sAction = $(this).attr('action'); //Ainsi que l'action pour savoir ou renvoyer
		var iNbChecked = $(this).find("input:checked").length;
		
		if(iNbChecked > 0) {	
			$.post(sAction, $(this).serialize(), function(aData) { //On POST les datas en ayant s�rializ� le formulaire
				$("#ft_content").empty().hide().html(aData).show(); //On affiche
			});
		} else {
		
			$.get($.get_host() + 'ajaxs/get_message_flash/WAR_PAS_DE_CDE', function(aData) { //On r�cup�re les donn�es en GET
				$(aData).insertAfter(".ft_footer_ext").show();						
				setTimeout(function() {
			        $(".flash_message_ext").fadeOut();
			    }, 1000);						
			});
			
		}
		return false; //Emp�che que le lien soit suivit par HTML
	});
	
	/*
	function countChecked() {
      
      $("div").text(n + (n == 1 ? " is" : " are") + " checked!");
    }
    countChecked();
    $(":checkbox").click(countChecked);
	*/
	/*******************/
	/*   FIN ETAPE 1   */
	/*******************/
	
	/***************/
	/*   ETAPE 2   */
	/***************/			
	$('#form_calcul_pv_suite').live('submit', function() {
		
		var sAction = $(this).attr('action'); //Ainsi que l'action pour savoir ou renvoyer
			
		$.post(sAction, $(this).serialize(), function(aData) { //On POST les datas en ayant s�rializ� le formulaire
			$("#ajax_content").empty().hide().html(aData).show(); //On affiche
		});
		
		return false; //Emp�che que le lien soit suivit par HTML
	});
	/*******************/
	/*   FIN ETAPE 2   */
	/*******************/
	
/***********************************************/
/*   FIN CALCUL PRIX DE VENTE ORDERS ET BILLS  */
/***********************************************/
		
});
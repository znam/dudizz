$(document).ready(function() {

	///////////////////
	//   CONNEXION   //
	
	$('#PersonneLoginForm').live('submit', function() {
		
		//L'utilisation de la m�thode live permet de mettre en place un �couteur sur la soumission du formulaire
		//ainsi m�me si le r�sultat d'une requ�te est effectu� via ajax la validation des formulaires sera toujours
		//effectu�e via AJAX
			
		var sFormId = $(this).attr('id'); //On r�cup�re l'id du formulaire
		var sAction = $(this).attr('action'); //Ainsi que l'action pour savoir ou renvoyer	
			
		$.post(sAction, $(this).serialize(), function(aData) { //On POST les datas en ayant s�rializ� le formulaire
		
			if(aData['status_requete'] == 0) { //Erreur dans le formulaire
				//AFFICHAGE DU MESSAGE D
				$.get($.get_host() + 'ajaxs/get_message_flash/' + aData['code_message'], function(aDataMessage) { //On r�cup�re les donn�es en GET
					$(aDataMessage).insertAfter(".connect_tableau").show();						
					setTimeout(function() {
				        $(".flash_message_ext").fadeOut();
				    }, 1500);						
				});	
			} else { //On redirige vers la page
				
				$(location).attr('href',aData['url']);
			}
		
		}, 'json'); //On demande � la m�thode POST un format de retour en JSON
		
		return false; //Emp�che que le formulaire soit valid� par HTML
	});
	///////////////////
	///////////////////	
});
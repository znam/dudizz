$(document).ready(function() {

/*************************/
/*   COLONNE DE DROITE   */
/*************************/
	//Visualiser
	var suppr_liens_detail = function () {
		var sUrlPage = $(this).attr('href'); //On r�cup�re le lien		
		$(this).removeAttr('href');
		$(this).attr('lien', sUrlPage);
	};	
	$("#details_cde_droite").livequery(suppr_liens_detail);
	
	$('#details_cde_droite').live("click", function() {
	
		var sUrlPage = $(this).attr('lien'); //On r�cup�re le lien		
		$(location).attr('href', sUrlPage);					
		return false; //Emp�che que le lien soit suivit par HTML
	});
	
	//Visualiser toutes
	var suppr_liens_historique = function () {
		var sUrlPage = $(this).attr('href') + '/fromMenu:1'; //On r�cup�re le lien		
		$(this).removeAttr('href');
		$(this).attr('lien', sUrlPage);
	};	
	$("#historique_cdes_droite").livequery(suppr_liens_historique);
	
	$('#historique_cdes_droite').live("click", function() {
	
		var sUrlPage = $(this).attr('lien'); //On r�cup�re le lien		
		$.get(sUrlPage, function(aData) { //On r�cup�re les donn�es en GET
			$("#ft_content").empty().hide().html(aData).show(); //On affiche
		});		
		return false; //Emp�che que le lien soit suivit par HTML
	});
	
	//Details des recaps livraison	
	$('#droite .ft_recap_cde_col').live("click", function() {	
		$('#droite .ft_recap_cde_col .fleches').removeClass('bas');
		$('#droite .ft_recap_cde_col .ft_infos_recap_livraison').slideUp('fast');
		$(this).find('.ft_infos_recap_livraison').slideDown('slow');
		$(this).find('.fleches').addClass('bas');
	});
	
	
	
/*****************************/
/*   FIN COLONNE DE DROITE   */
/*****************************/

/******************/
/*   HISTORIQUE   */
/******************/
	//Formulaire de tri
	$('#form_orders_historique').live('submit', function() {
			
		var sFormId = $(this).attr('id'); //On r�cup�re l'id du formulaire			
		var sAction = $(this).attr('action'); //Ainsi que l'action pour savoir ou renvoyer
			
		$.post(sAction, $(this).serialize(), function(aData) { //On POST les datas en ayant s�rializ� le formulaire
			$("#ajax_content").empty().hide().html(aData).show(); //On affiche
		});
		return false; //Emp�che que le lien soit suivit par HTML
	
	});
	
	//Suppression des liens
	var suppr_liens_orders_historiques = function () {
		var sUrlPage = $(this).attr('href'); //On r�cup�re le lien		
		$(this).removeAttr('href');
		$(this).attr('lien', sUrlPage);
	};	
	$("#ft_orders_historique a").livequery(suppr_liens_orders_historiques);
	
	$('#ft_orders_historique a').live("click", function() {
	
		var sUrlPage = $(this).attr('lien'); //On r�cup�re le lien		
		$(location).attr('href', sUrlPage);					
		return false; //Emp�che que le lien soit suivit par HTML
	});
/**********************/
/*   FIN HISTORIQUE   */
/**********************/

/*************************/
/*   FORMULAIRE DE TRI   */
/*************************/
	$('#form_propositions_details_liste').live('submit', function() {
			
		var sFormId = $(this).attr('id'); //On r�cup�re l'id du formulaire			
		var sAction = $(this).attr('action'); //Ainsi que l'action pour savoir ou renvoyer
			
		$.post(sAction, $(this).serialize(), function(aData) { //On POST les datas en ayant s�rializ� le formulaire
			$("#ajax_content").empty().hide().html(aData).show(); //On affiche
		});
		return false; //Emp�che que le lien soit suivit par HTML
	
	});	
/*****************************/
/*   FIN FORMULAIRE DE TRI   */
/*****************************/

/*************************/
/*   AJOUT DE QUANTITE   */
/*************************/				
	$('.affichage_propositions_details form').live('submit', function() {
		
		//L'utilisation de la m�thode live permet de mettre en place un �couteur sur la soumission du formulaire
		//ainsi m�me si le r�sultat d'une requ�te est effectu� via ajax la validation des formulaires sera toujours
		//effectu�e via AJAX
			
		var sFormId = $(this).attr('id'); //On r�cup�re l'id du formulaire
		var sAction = $(this).attr('action'); //Ainsi que l'action pour savoir ou renvoyer
			
		$.post(sAction, $(this).serialize(), function(aData) { //On POST les datas en ayant s�rializ� le formulaire			
			$.each(aData, function(sEntryIndex, aEntry) { //On parcour le r�sultat
				if(sEntryIndex == "PropositionsDetail") { //Si on est sur le noeud PropositionsDetail
											
					//Gestion de l'affichage de la quantit� command�e
					var sQteCdee = "#" + sFormId + " .qte_commandee b";
					$(sQteCdee).html(aEntry['qte_commandee']);
					
					//Dans le cas des opportunit�s on aura une quantit� restante >= 0
					if(aEntry['qte_restante'] >= 0) {		
						//Gestion de la quantit� restante
						var sQteRestante = "#" + sFormId + " .stock b";
						$(sQteRestante).html("Stock " + aEntry['qte_restante']);						
						
						//Gestion de la jauge
						var sJauge = "#" + sFormId + " .ft_jauge";
						var iValeurJauge = $(sJauge).html();
						var sNewClassJauge = "ft_jauge_n" + aEntry['jauge'];
						$(sJauge).removeClass("ft_jauge_n" + iValeurJauge);
						$(sJauge).addClass(sNewClassJauge);
						$(sJauge).html(aEntry['jauge']);
						
						//Si il n'y a plus de quantit� disponible en stock il faut supprimer le formulaire
						if(aEntry['qte_restante'] == 0 && aEntry['is_promo'] == 1) {
							var sPlusDeStock = "#" + sFormId + " .bas";
							$(sPlusDeStock).empty().html(aEntry['epuise']);
						}						
					}
					
					var sEnplacementBouton = "#" + sFormId + " .bas .bouton input";						
					if(aEntry['is_promo'] == 1) {
						$(sEnplacementBouton).attr("value", aEntry['libelle_ajouter']);
					} else {
						if(aEntry['qte_commandee'] == 0) {
							$(sEnplacementBouton).attr("value", aEntry['libelle_commander']);
						} else {
							$(sEnplacementBouton).attr("value", aEntry['libelle_modifier']);
						}
					}
					
				} else if(sEntryIndex == "Conditionnement") { //Si on est sur le noeud Conditionnement
					$('.recap_livraison').empty().hide().html(aEntry['html']).fadeIn();
				}
				//MESSAGE DE CONFIRMATION
				/*
				$.get($.get_host() + 'ajaxs/get_message_flash/UPD', function(aData) { //On r�cup�re les donn�es en GET
					$(aData).insertAfter(".ft_footer_ext").show();						
					setTimeout(function() {
				        $(".flash_message_ext").fadeOut();
				    }, 1000);						
				});*/					
			});	
		}, 'json'); //On demande � la m�thode POST un format de retour en JSON
		
		return false; //Emp�che que le formulaire soit valid� par HTML
	});
/*****************************/
/*   FIN AJOUT DE QUANTITE   */
/*****************************/

/****************************/
/*   REPARTIR LA COMMANDE   */
/****************************/	
	//Suppression de tous les liens href
	var suppr_liens_home = function () {
		var sUrlPage = $(this).attr('href') + '/fromMenu:1'; //On r�cup�re le lien		
		$(this).removeAttr('href');
		$(this).attr('lien', sUrlPage);
	};	
	$("#repartir_cde").livequery(suppr_liens_home);
	
	$('#repartir_cde').live("click", function() {
	
		var sUrlPage = $(this).attr('lien'); //On r�cup�re le lien		
		
		$.get(sUrlPage, function(aData) { //On r�cup�re les donn�es en GET
			$("#ft_content").empty().hide().html(aData).show(); //On affiche
		});
					
		return false; //Emp�che que le lien soit suivit par HTML
	});

	$('#form_repartir_cde').live('submit', function() {
		
		var sAction = $(this).attr('action'); //Ainsi que l'action pour savoir ou renvoyer
			
		$.post(sAction, $(this).serialize(), function(aData) { //On POST les datas en ayant s�rializ� le formulaire
			$("#ajax_content").empty().hide().html(aData).show(); //On affiche
		});
		
		return false; //Emp�che que le lien soit suivit par HTML
	});
	
	//Suppression de tous les liens href
	var suppr_liens_home = function () {
		var sUrlPage = $(this).attr('href'); //On r�cup�re le lien		
		$(this).removeAttr('href');
		$(this).attr('lien', sUrlPage);
	};	
	$("#ft_orders_repartition a").livequery(suppr_liens_home);
	
	$('#ft_orders_repartition a').live("click", function() {
	
		var sUrlPage = $(this).attr('lien'); //On r�cup�re le lien		
		$(location).attr('href', sUrlPage);					
		return false; //Emp�che que le lien soit suivit par HTML
	});
/********************************/
/*   FIN REPARTIR LA COMMANDE   */
/********************************/

});
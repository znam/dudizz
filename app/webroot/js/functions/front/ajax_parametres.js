$(document).ready(function() {

/******************/
/*   MON COMPTE   */
/******************/
	$('#form_parametres_mon_compte').live('submit', function() {
			
		var sFormId = $(this).attr('id'); //On r�cup�re l'id du formulaire			
		var sAction = $(this).attr('action'); //Ainsi que l'action pour savoir ou renvoyer
			
		$.post(sAction, $(this).serialize(), function(aData) { //On POST les datas en ayant s�rializ� le formulaire
			$("#ajax_content").empty().hide().html(aData).show(); //On affiche
		});
		return false; //Emp�che que le lien soit suivit par HTML
	
	});
/**********************/
/*   FIN MON COMPTE   */
/**********************/

/*****************/
/*   MES ACCES   */
/*****************/
	$('#form_parametres_mes_acces_mon_acces').live('submit', function() {
			
		var sFormId = $(this).attr('id'); //On r�cup�re l'id du formulaire			
		var sAction = $(this).attr('action'); //Ainsi que l'action pour savoir ou renvoyer
			
		$.post(sAction, $(this).serialize(), function(aData) { //On POST les datas en ayant s�rializ� le formulaire
			$("#ajax_content").empty().hide().html(aData).show(); //On affiche
		});
		return false; //Emp�che que le lien soit suivit par HTML
	
	});
	
	$('#form_parametres_mes_acces_mon_email').live('submit', function() {
			
		var sFormId = $(this).attr('id'); //On r�cup�re l'id du formulaire			
		var sAction = $(this).attr('action'); //Ainsi que l'action pour savoir ou renvoyer
			
		$.post(sAction, $(this).serialize(), function(aData) { //On POST les datas en ayant s�rializ� le formulaire
			$("#ajax_content").empty().hide().html(aData).show(); //On affiche
		});
		return false; //Emp�che que le lien soit suivit par HTML
	
	});
	
	//Suppression du lien cr�er acces
	var suppr_liens_orders_historiques = function () {
		var sUrlPage = $(this).attr('href') + '/fromMenu:1'; //On r�cup�re le lien		
		$(this).removeAttr('href');
		$(this).attr('lien', sUrlPage);
	};	
	$("#parametres_mes_acces_creer_acces").livequery(suppr_liens_orders_historiques);
	
	$('#parametres_mes_acces_creer_acces').live("click", function() {
	
		var sUrlPage = $(this).attr('lien'); //On r�cup�re le lien		
		$.get(sUrlPage, function(aData) { //On r�cup�re les donn�es en GET
			$("#ft_content").empty().hide().html(aData).show(); //On affiche
		});
		return false; //Emp�che que le lien soit suivit par HTML
	});
	
	//Suppression des liens du tableau
	var suppr_liens_orders_historiques = function () {
		
		var sCss = $(this).attr('class'); //On r�cup�re le css
		var sUrlPage = $(this).attr('href'); //On r�cup�re le lien
		if(sCss == "edit") { sUrlPage = sUrlPage + '/fromMenu:1'; }		
		$(this).removeAttr('href');
		$(this).attr('lien', sUrlPage);
	};	
	$(".ft_tableau_acces a").livequery(suppr_liens_orders_historiques);
	
	$('.ft_tableau_acces a').live("click", function() {
	
		var sUrlPage = $(this).attr('lien'); //On r�cup�re le lien		
		var sCss = $(this).attr('class'); //On r�cup�re le css		
		var sIdLigne = $(this).parent().parent().attr('id');
		
		$.get(sUrlPage, function(aData) { //On r�cup�re les donn�es en GET
			if(sCss == "edit") {
				$("#ft_content").empty().hide().html(aData).show(); //On affiche
			} else {			
					
				if(aData == 1) {
				
					$.get($.get_host() + 'ajaxs/get_message_flash/DEL', function(aData) { //On r�cup�re les donn�es en GET
						$(aData).insertAfter(".ft_footer_ext").show();						
						setTimeout(function() {
					        $(".flash_message_ext").fadeOut();
					    }, 1000);						
					});
					$("#" + sIdLigne).remove();
					
				} else {
					
					$.get($.get_host() + 'ajaxs/get_message_flash/ERR', function(aData) { //On r�cup�re les donn�es en GET
						$(aData).insertAfter(".ft_footer_ext").show();						
						setTimeout(function() {
					        $(".flash_message_ext").fadeOut();
					    }, 1000);						
					});
					
				}
			}
		});
		return false; //Emp�che que le lien soit suivit par HTML
	});
	
	//Ajout - Edition d'un utilisateur
	$('#form_utilisateur_add_edit').live('submit', function() {
			
		var sFormId = $(this).attr('id'); //On r�cup�re l'id du formulaire			
		var sAction = $(this).attr('action'); //Ainsi que l'action pour savoir ou renvoyer
			
		$.post(sAction, $(this).serialize(), function(aData) { //On POST les datas en ayant s�rializ� le formulaire
			
			var iResult = aData['result'];
			$(".ft_centre").empty().hide().html(aData['html']).show(); //On affiche
			if(iResult == 1) {
				$.get($.get_host() + 'ajaxs/get_message_flash/UPD', function(aData) { //On r�cup�re les donn�es en GET
					$(aData).insertAfter(".ft_footer_ext").show();						
					setTimeout(function() {
				        $(".flash_message_ext").fadeOut();
				    }, 1000);						
				});
			}
		}, 'json');
		return false; //Emp�che que le lien soit suivit par HTML
	
	});
	
/*********************/
/*   FIN MES ACCES   */
/*********************/

});
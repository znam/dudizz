$(document).ready(function() {

/*********************************************************/
/*   GESTION DU CLICK SUR LE LIBELLE DE LA PROPOSITION   */
/*********************************************************/	
	var suppr_liens_proposition = function () {
		var sUrlPage = $(this).attr('href') + "/fromOther:1"; //On r�cup�re le lien
		$(this).removeAttr('href');
		$(this).attr('lien', sUrlPage);
	};	
	$(".affichage_propositions a").livequery(suppr_liens_proposition);
	
	$(".affichage_propositions a").live('click', function() { //Tous les liens de la home sauf ceux de la pagination (g�r� plus bas)
		
		var sUrlPage = $(this).attr('lien'); //On r�cup�re le lien
			
		$.get(sUrlPage, function(aData) { //On r�cup�re les donn�es en GET
			$("#ft_content").empty().hide().html(aData).show();
			
		});
		
		return false; //Emp�che que le lien soit suivit par HTML
	});	
/*************************************************************/
/*   FIN GESTION DU CLICK SUR LE LIBELLE DE LA PROPOSITION   */
/*************************************************************/

/***********************************************/
/*   SURVOL DES VIGNETTES LISTE PROPOSITIONS   */
/***********************************************/
	$(".top_5_produits .grid_1").live('mouseover', function () { 
		$(this).children(".zoom").show(); 
	}).live('mouseout', function () { 
		$(this).children(".zoom").hide(); 
	});
/***************************************************/
/*   FIN SURVOL DES VIGNETTES LISTE PROPOSITIONS   */
/***************************************************/

});
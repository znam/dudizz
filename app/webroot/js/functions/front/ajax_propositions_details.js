$(document).ready(function() {

/*************************************/
/*   GESTIONNAIRE DE LA PAGINATION   */
	//Suppression de tous les liens href
	var suppr_liens_pagin = function () {
		var sUrlPage = $(this).attr('href'); //On r�cup�re le lien
		$(this).removeAttr('href');
		$(this).attr('lien', sUrlPage);
	};	
	$(".affichage_propositions_details .ft_pagin a").livequery(suppr_liens_pagin);
		
	$('.affichage_propositions_details .ft_pagin a').live('click', function() {
	
		//L'utilisation de la m�thode live permet de mettre en place un �couteur sur le click
		//ainsi m�me si le r�sultat d'une requ�te est effectu� via ajax le click sur un lien de la pagination
		//sera toujours effectu� via AJAX
		var sUrlPage = $(this).attr('lien'); //On r�cup�re le lien
				
		$.get(sUrlPage, function(aData) { //On r�cup�re les donn�es en GET
			$("#ajax_content").empty().hide().html(aData).show(); //On affiche
		});			
		return false; //Emp�che que le lien soit suivit par HTML
	});
/*************************************/

});
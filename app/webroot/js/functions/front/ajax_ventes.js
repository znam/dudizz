$(document).ready(function() {
	
/*********************/
/*   SAISIES HEBDO   */
/*********************/
	var suppr_saisies_hebdo = function () {
		var sUrlPage = $(this).attr('href'); //On r�cup�re le lien
		$(this).removeAttr('href');
		$(this).attr('lien', sUrlPage);
	};	
	$("#ft_ventes_saisie_hebdo a").livequery(suppr_saisies_hebdo);
	
	$("#ft_ventes_saisie_hebdo a").live('click', function() { //Tous les liens de la home sauf ceux de la pagination (g�r� plus bas)
		
		var sUrlPage = $(this).attr('lien'); //On r�cup�re le lien
			
		$.get(sUrlPage, function(aData) { //On r�cup�re les donn�es en GET
			$("#ajax_content").empty().hide().html(aData).show(); //On affiche
		});	
		return false; //Emp�che que le lien soit suivit par HTML
	});	
	
	$('#form_saisies_hebdo').live('submit', function() {
		
		var sAction = $(this).attr('action'); //Ainsi que l'action pour savoir ou renvoyer
			
		$.post(sAction, $(this).serialize(), function(aData) { //On POST les datas en ayant s�rializ� le formulaire
			$("#ajax_content").empty().hide().html(aData).show(); //On affiche
		});
		
		return false; //Emp�che que le lien soit suivit par HTML
	});		
/*************************/
/*   FIN SAISIES HEBDO   */
/*************************/
		
/******************/
/*   HISTORIQUE   */
/******************/
	var make_accordion = function () {
		$(this).accordion();
		$(".ft_accordion .ui-accordion-content").css("height", "auto");
	};	
	$(".ft_accordion").livequery(make_accordion);
	
	var suppr_liens_historique_ventes = function () {
		var sUrlPage = $(this).attr('href'); //On r�cup�re le lien
		$(this).removeAttr('href');
		$(this).attr('lien', sUrlPage);
	};	
	$(".ft_accordion a").livequery(suppr_liens_historique_ventes);
			
	$('#form_historique_ventes').live('submit', function() {
			
		var sFormId = $(this).attr('id'); //On r�cup�re l'id du formulaire
		var sAction = $(this).attr('action'); //Ainsi que l'action pour savoir ou renvoyer
			
		$.post(sAction, $(this).serialize(), function(aData) { //On POST les datas en ayant s�rializ� le formulaire
			$("#ajax_content").empty().hide().html(aData).show(); //On affiche				
		});
		
		return false; //Emp�che que le lien soit suivit par HTML
	});	
/**********************/
/*   FIN HISTORIQUE   */
/**********************/
		
/*******************/
/*   COMPARATIF   */
/*******************/				
	$('#form_comparatif').live('submit', function() {
			
		var sFormId = $(this).attr('id'); //On r�cup�re l'id du formulaire			
		var sAction = $(this).attr('action'); //Ainsi que l'action pour savoir ou renvoyer
			
		$.post(sAction, $(this).serialize(), function(aData) { //On POST les datas en ayant s�rializ� le formulaire
			$("#ajax_content").empty().hide().html(aData).show(); //On affiche				
		});
		
		return false; //Emp�che que le lien soit suivit par HTML
	});	
/**********************/
/*   FIN COMPARATIF   */
/**********************/
		
/***********************/
/*   CHIFFRES RESEAU   */
/***********************/

	//Chiffres r�seau
	$('#form_chiffre_reseau').live('submit', function() {
			
		var sFormId = $(this).attr('id'); //On r�cup�re l'id du formulaire						
		var sAction = $(this).attr('action'); //Ainsi que l'action pour savoir ou renvoyer
			
		$.post(sAction, $(this).serialize(), function(aData) { //On POST les datas en ayant s�rializ� le formulaire
			$("#ajax_content").empty().hide().html(aData).show(); //On affiche				
		});
		
		return false; //Emp�che que le lien soit suivit par HTML
	});	
	
	var suppr_liens_chiffres_reseau = function () {
		var sUrlPage = $(this).attr('href') + "/fromChiffres:1"; //On r�cup�re le lien
		$(this).removeAttr('href');
		$(this).attr('lien', sUrlPage);
	};	
	$("#ft_ventes_chiffres_reseau a").livequery(suppr_liens_chiffres_reseau);
	
	$("#ft_ventes_chiffres_reseau a").live('click', function() { //Tous les liens de la home sauf ceux de la pagination (g�r� plus bas)
		
		var sUrlPage = $(this).attr('lien'); //On r�cup�re le lien
		$.get(sUrlPage, function(aData) { //On r�cup�re les donn�es en GET
			$(".ft_centre").empty().hide().html(aData).show(); //On affiche
		});	
		return false; //Emp�che que le lien soit suivit par HTML
	});	
	
	//Chiffres r�seau magasin	
	$('#form_chiffre_reseau_magasin').live('submit', function() {
			
		var sFormId = $(this).attr('id'); //On r�cup�re l'id du formulaire						
		var sAction = $(this).attr('action'); //Ainsi que l'action pour savoir ou renvoyer
			
		$.post(sAction, $(this).serialize(), function(aData) { //On POST les datas en ayant s�rializ� le formulaire
			$("#ajax_content").empty().hide().html(aData).show(); //On affiche				
		});
		
		return false; //Emp�che que le lien soit suivit par HTML
	});	
	
	var suppr_liens_chiffres_reseau = function () {
		var sUrlPage = $(this).attr('href') + "/fromMenu:1"; //On r�cup�re le lien
		$(this).removeAttr('href');
		$(this).attr('lien', sUrlPage);
	};	
	$("#ft_ventes_chiffres_reseau_magasin a").livequery(suppr_liens_chiffres_reseau);
	
	$("#ft_ventes_chiffres_reseau_magasin a").live('click', function() { //Tous les liens de la home sauf ceux de la pagination (g�r� plus bas)
		
		var sUrlPage = $(this).attr('lien'); //On r�cup�re le lien
		$.get(sUrlPage, function(aData) { //On r�cup�re les donn�es en GET
			$("#ft_content").empty().hide().html(aData).show(); //On affiche
		});	
		return false; //Emp�che que le lien soit suivit par HTML
	});	
/***************************/
/*   FIN CHIFFRES RESEAU   */
/***************************/

});
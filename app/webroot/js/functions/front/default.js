$(document).ready(function() {
		
	$(".ft_js_ext").hide(); //On cache le message quand le js est active
	
	/*GESTION DE LA LISTE DES MAGASINS*/		
	$(".ft_list_mags ul").hide();	//On cache tous les sous menus	
	$(".ft_list_mags").mouseover(function() { $(".ft_list_mags ul").show(); }).mouseout(function() { $(".ft_list_mags ul").hide(); });
	
	/*GESTION DES SOUS MENUS*/		
	$(".ft_sous_menu").hide();	//On cache tous les sous menus	
	$(".ft_menu").mouseover(function() { 
		$(this).children(".ft_sous_menu").show();
	}).mouseout(function() { 
		$(".ft_sous_menu").hide(); 
	});	
	
	
/*******************************/
/*   FORMULAIRE DE CONNEXION   */
  /*******************************/
  
    //SUPPRESSION DU MESSAGE SI LE JS EST ACTIVE
	$(".ligne_js").hide();
	
	//AFFICHAGE DU FORMULAIRE DE RECUP DU MDP
	$(".lost").toggle(
		function () { $(".form_lost").show(); },
		function () { $(".form_lost").hide(); }		
	);
	
	//FORMULAIRE DE CONNEXION A L'INTRANET
	var sDebForm = '<form action="' + $.get_host() + '" method="post" id="PersonneLoginForm">';
	var sHiddenForm = '<input type="hidden" value="POST" name="_method"/><input type="hidden" id="UserTypeAction" value="login" name="data[User][type_action]"/>';
	var sFinForm = '</form>';
	
	var sContentForm = sDebForm + sHiddenForm + $('.connect_content').html() + sFinForm;
	$('.connect_content').html(sContentForm);
	
	//FORMULAIRE DE NOUVEAU MOT DE PASSE	
	var sDebFormMdp = '<form action="' + $.get_host() + '" method="post" id="PersonneLoginForm">';
	var sHiddenFormMdp = '<input type="hidden" value="POST" name="_method"/><input type="hidden" id="UserTypeAction" value="lost_passwd" name="data[User][type_action]"/>';
	var sFinFormMdp = '</form>';
	
	var sContentFormMdp = sDebFormMdp + sHiddenFormMdp + $('.mdp_content').html() + sFinFormMdp;
	$('.mdp_content').html(sContentFormMdp);
	
/***********************************/
/*   FIN FORMULAIRE DE CONNEXION   */
   /***********************************/	
	
});
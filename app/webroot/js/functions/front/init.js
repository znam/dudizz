function noBack() { window.history.forward(); }
noBack();
window.onload = noBack;
window.onpageshow = function(evt) { if(evt.persisted) { noBack(); } }
window.onunload = function() { void(0); }

$(document).ready(function() {
	jQuery.extend({
		get_host: function() { 
			var sHostToReturn = "http://";
	  		var sHost = window.location.host;
	  		
	  		if(sHost == "127.0.0.1") { sHostToReturn =  sHostToReturn + sHost + "/intranet2.groupemonceaufleurs.com/"; }
	  		else { sHostToReturn =  sHostToReturn + sHost + "/"; }
	  		
	  		return sHostToReturn; 
		},
		get_surface_ecran: function() {
			var oDivFooter = $('.bas_page'); //Mise en variable de l'�l�ment footer
			var iHeightDivFooter = oDivFooter.height(); //r�cup�ration de la hauteur de l'�l�ment
			var aOffsetDivFooter = oDivFooter.offset(); //R�cup�ration du top et du left
			var iTopDivFooter = aOffsetDivFooter.top; //R�cup�ration du top
			var iScreenWidth = screen.width; //Longueur de l'�cran
			
			var iWidthLoading = iScreenWidth;
			var iHeightLoading = iTopDivFooter + iHeightDivFooter;
			
			return  {width: iScreenWidth + "px", height: iHeightLoading + "px"};
		}		
	});
});